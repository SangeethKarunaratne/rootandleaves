import React, { Fragment } from 'react';
import Head from 'next/head';
import { ThemeProvider } from 'styled-components';
import Sticky from 'react-stickynode';
import { DrawerProvider } from '../packages/common_2/src/contexts/DrawerContext';
import { saasThreeTheme } from '../packages/common_2/src/theme/saasThree';
import { ResetCSS } from '../packages/common_2/src/assets/css/style';
import {
  GlobalStyle,
  ContentWrapper,
} from '../packages/common_2/src/containers/SaasThree/saasThree.style';

import BannerSection from '../packages/common_2/src/containers/SaasThree/Banner';
import Navbar from '../packages/common_2/src/containers/SaasThree/Navbar';
import ServiceSection from '../packages/common_2/src/containers/SaasThree/Service';
import PricingSection from '../packages/common_2/src/containers/SaasThree/Pricing';
import PartnerSection from '../packages/common_2/src/containers/SaasThree/Partner';
import TrialSection from '../packages/common_2/src/containers/SaasThree/Trial';
import FeatureSection from '../packages/common_2/src/containers/SaasThree/Feature';
import UpdateScreen from '../packages/common_2/src/containers/SaasThree/UpdateScreen';
import TestimonialSection from '../packages/common_2/src/containers/SaasThree/Testimonial';
import Newsletter from '../packages/common_2/src/containers/SaasThree/Newsletter';
import Footer from '../packages/common_2/src/containers/SaasThree/Footer';

const SaasClassic = () => {
  return (
    <ThemeProvider theme={saasThreeTheme}>
      <Fragment>
        <Head>
          <title>SaaS | A react next landing page</title>
          <meta name="Description" content="React next landing page" />
          <meta name="theme-color" content="#ec5555" />
          {/* Load google fonts */}
          <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,900|Open+Sans:400,400i,600,700"
            rel="stylesheet"
          />
        </Head>

        <ResetCSS />
        <GlobalStyle />

        <ContentWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar />
            </DrawerProvider>
          </Sticky>
          <BannerSection />
          <ServiceSection />
          <UpdateScreen />
          <FeatureSection />
          <PartnerSection />
          <PricingSection />
          <TestimonialSection />
          <TrialSection />
          <Newsletter />
          <Footer />
        </ContentWrapper>
      </Fragment>
    </ThemeProvider>
  );
};

export default SaasClassic;
