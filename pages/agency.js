import React, { Fragment } from 'react';
import Head from 'next/head';
import Sticky from 'react-stickynode';
import { ThemeProvider } from 'styled-components';
import { agencyTheme } from '../packages/common_2/src/theme/agency';
import { ResetCSS } from '../packages/common_2/src/assets/css/style';
import {
  GlobalStyle,
  AgencyWrapper,
} from '../packages/common_2/src/containers/Agency/agency.style';
import Navbar from '../packages/common_2/src/containers/Agency/Navbar';
import BannerSection from '../packages/common_2/src/containers/Agency/BannerSection';
import FeatureSection from '../packages/common_2/src/containers/Agency/FeatureSection';
import AboutUsSection from '../packages/common_2/src/containers/Agency/AboutUsSection';
import WorkHistory from '../packages/common_2/src/containers/Agency/WorkHistory';
import BlogSection from '../packages/common_2/src/containers/Agency/BlogSection';
import TestimonialSection from '../packages/common_2/src/containers/Agency/TestimonialSection';
import TeamSection from '../packages/common_2/src/containers/Agency/TeamSection';
import VideoSection from '../packages/common_2/src/containers/Agency/VideoSection';
import NewsletterSection from '../packages/common_2/src/containers/Agency/NewsletterSection';
import QualitySection from '../packages/common_2/src/containers/Agency/QualitySection';
import Footer from '../packages/common_2/src/containers/Agency/Footer';
import { DrawerProvider } from '../packages/common_2/src/contexts/DrawerContext';
//import FaqSection from '../packages/common_2/src/containers/Agency/FaqSection';

const Agency = () => {
  return (
    <ThemeProvider theme={agencyTheme}>
      <Fragment>
        {/* Start agency head section */}
        <Head>
          <title>Agency | A react next landing page</title>
          <meta name="Description" content="React next landing page" />
          <meta name="theme-color" content="#10ac84" />

          {/* Load google fonts */}
          <link
            href="https://fonts.googleapis.com/css?family=Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i"
            rel="stylesheet"
          />
        </Head>
        <ResetCSS />
        <GlobalStyle />
        {/* End of agency head section */}
        {/* Start agency wrapper section */}
        <AgencyWrapper>
          <Sticky top={0} innerZ={9999} activeClass="sticky-nav-active">
            <DrawerProvider>
              <Navbar />
            </DrawerProvider>
          </Sticky>
          <BannerSection />
          <FeatureSection />
          <AboutUsSection />
          <WorkHistory />
          <BlogSection />
          <QualitySection />
          <VideoSection />
          <TestimonialSection />
          <TeamSection />
          {/*<FaqSection />*/}
          <NewsletterSection />
          <Footer />
        </AgencyWrapper>
        {/* End of agency wrapper section */}
      </Fragment>
    </ThemeProvider>
  );
};

export default Agency;
